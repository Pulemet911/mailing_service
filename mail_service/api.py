from rest_framework import viewsets
from mail_service.models import Mailing, Client, Message
from mail_service.serializers import MailinglistSerializer, MessageSerializer, ClientSerializer, MailingSerializer
from django_filters.rest_framework import DjangoFilterBackend


class MailingView(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return MailinglistSerializer
        return MailingSerializer


class ClientView(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('prefix', 'tag')


class MessageView(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
