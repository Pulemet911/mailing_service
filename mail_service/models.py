import pytz
from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator


class Mailing(models.Model):
    """Модель отправки"""
    date_start = models.DateField(verbose_name='дата начала')
    date_end = models.DateField(verbose_name='дата окончания')
    start_time = models.TimeField(verbose_name='время запуска рассылки')
    finish_time = models.TimeField(verbose_name='время окончания рассылки')
    message_text = models.TextField(verbose_name='текст сообщения')
    prefix = models.CharField('код мобильного оператора', max_length=50, blank=True)
    tag = models.CharField(max_length=33, unique=True, verbose_name='тег')

    @property
    def to_send(self):
        now = timezone.now()
        if self.start_time <= now <= self.finish_time:
            return True
        else:
            return False


    def __str__(self):
        return f'Рассылка {self.id}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    """Модель клиента"""
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    validator = RegexValidator(regex=r'7[0-9]{10}', message='формат ввода: 7XXXXXXXXXX (X - цифра от 0 до 9)')
    phone_number = models.PositiveIntegerField(verbose_name='номер телефона', validators=[validator])
    prefix = models.PositiveIntegerField(verbose_name='код мобильного оператора')
    tag = models.CharField(max_length=33, unique=True, verbose_name='тег')
    timezone = models.CharField(verbose_name='часовой пояс', max_length=32, choices=TIMEZONES, default='UTC')

    def save(self, *args, **kwargs):
        self.prefix = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self):
        return f'Клиент {self.id} с номером телефона {self.phone_number}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    """Модель сообщения"""
    CHOICES = (
        ('S', 'send'),
        ('N', 'not send'),
    )

    date = models.DateTimeField(verbose_name='дата отправки', auto_now_add=True)
    status = models.CharField(verbose_name='статус отправки', max_length=15, choices=CHOICES)
    message = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')

    def __str__(self):
        return f'Сообщение {self.id} с текстом {self.message} для {self.client}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

