from rest_framework import serializers
from mail_service.models import Mailing, Client, Message


class MessageSerializer(serializers.ModelSerializer):
    """Сериалайзер сообщения"""

    class Meta:
        model = Message
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    """Сериалайзер отправки"""
    messages = MessageSerializer(read_only=True, many=True)

    class Meta:
        model = Mailing
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    """Сериалайзер клиента"""

    class Meta:
        model = Client
        fields = '__all__'
