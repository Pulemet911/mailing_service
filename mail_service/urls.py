from django.urls import include, path
from rest_framework.routers import DefaultRouter

from mail_service.api import MailingView, ClientView, MessageView

router = DefaultRouter()
router.register(r'mailing', MailingView)
router.register(r'client', ClientView)
router.register(r'message', MessageView)


urlpatterns = [
    path('v1/', include(router.urls)),
    ]
