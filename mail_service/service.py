# from apscheduler.schedulers.background import BackgroundScheduler
# from apscheduler.triggers.cron import CronTrigger
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from django.utils import timezone
#
# from mail_service.models import Mailing
# from mail_service.tasks import send_message
#
#
# @receiver(signal=post_save, sender=Mailing)
# def mailing_request(sender, instance=None, created=False, **kwargs):
#     if created:
#         scheduler = BackgroundScheduler()
#         scheduler.start()
#         if instance.to_send:
#             scheduler.add_job(
#                 send_message,
#                 trigger=CronTrigger(start_date=timezone.localtime(timezone.now())),
#                 args=[instance.id],
#                 id=f"_job",
#                 max_instances=1,
#                 replace_existing=True,
#             )
#         else:
#             scheduler.add_job(
#                 send_message,
#                 trigger=CronTrigger(start_date=instance.beginning),
#                 args=[instance.id],
#                 id=f"__job",
#                 max_instances=1,
#                 replace_existing=True,
#             )

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q
from mail_service.models import *
from mail_service.tasks import send_message


@receiver(post_save, sender=Mailing)
def create_message(sender, instance, created, **kwargs):
    if created:
        mail = Mailing.objects.filter(id=instance.id).first()
        clients = Client.objects.filter(Q(mobile_operator_code=mail.prefix) |
                                        Q(tag=mail.tag)).all()

        for client in clients:
            Message.objects.create(
                sending_status='Не доставлено',
                client_id=client.id,
                distribution_id=instance.id,
            )
            message = Message.objects.filter(distribution_id=instance.id, client_id=client.id).first()
            data = {
                'id': message.id,
                'phone': client.phone_number,
                'text': mail.text,
            }
            client_id = client.id
            mail_id = mail.id

            if instance.to_send:
                send_message.apply_async(
                    (data, client_id, mail_id),
                    expires=mail.finish_time,
                )
            else:
                send_message.apply_async(
                    (data, client_id, mail_id),
                    eta=mail.start_time,
                    expires=mail.finish_time,
                )

